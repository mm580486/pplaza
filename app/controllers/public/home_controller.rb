class Public::HomeController < ApplicationController
    layout 'public'
    before_action :exec_setting
    before_action :check_development_mod,except: :update
    skip_before_filter :verify_authenticity_token, :only => [:callBackUpgrade]

    def reset_password
       if User.exists?(phone: params[:phone])
           @user=User.find_by_phone(params[:phone])
           password=rand(10000000...99999999)
           @user.update_attributes(password: password,password_confirmation: password)
           
           client=KaveRestApi::SendSimple.new({
            receptor: @user.phone, # can be array ['09127105568','09123456789'] < = 3000 
            message: "رمز عبور شما در نیازمندی های آنلاین پلازا
            #{password}
            "
            }).call
            flash[:notice]=[5000,'رمز عبور جدید ارسال شد']
            redirect_to :back
       else
                 flash[:notice]=[5000,'کاربری با این شماره وجود ندارد']
            redirect_to :back 
       end
        
        
    end

    def show_exposition
        render json: {status: 'salam'}
    end
    
    def index
        @products=Product.all.where(accept: true).where(['created_at > ?', 30.days.ago])
        @off_products=Product.all.where.not(off_price: '').limit(8)
        @expositions=User.sellers.limit(8)
        @categories = Category.where(parent_id: nil)
        @dossiers= Dossier.all
        @shuffleProducts=Product.where(fori: true).order("RANDOM()").limit(6)
        if @shuffleProducts.size < 3
            @shuffleProducts = Product.order('view_product DESC').limit(6)
        end


    end
    
    def update
      UserMailer.welcome_email(User.first).deliver_later
    end
    
    def category
      @categories = Category.where(parent_id: nil)  
      @category=Category.find_by_permalink(params[:permalink])
      cate= []
      cate << @category.id
      cate += @category.subcategories.ids
      @products=Product.where(category_id: cate )
    end
    
    def register_exposition
        @user = User.new
    end
    
    def following
        @expositions = current_user.following
    end
    
    def following_post
        @products = Product.where(user_id: current_user.following)
    end
    
    def product
        @comment=Comment.new
        @product=Product.find(params[:id])
        @exposition=@product.user
        @product.view_product = @product.view_product.to_i + 1
        @product.update_attribute(:view_product,@product.view_product)
    end


    def new_product
        

        if params[:id]
            @product=Product.new(product_type_id: Category.find(params[:id]).product_type_id)
        else
            @product=Product.new

        end
          
    end


    def getFields
        @product_fields=ProductField.where("? = ANY (categories)",params[:id].to_s)
    end

    def create_product
      if current_user.nil?
            if User.find_by_phone(params[:phone])
            u=User.find_by_phone(params[:phone])
        else
            u=User.new(phone: params[:phone],email: params[:email],exposition_address: params[:exposition_address],name: '')
            u.save
        end
    else 
        u=current_user
    end

        @product=u.products.new(product_white_list)
        @product.city = params[:city]
        @product.state = params[:state]
        @product.product_type_id=Category.find(params[:product][:category_id]).product_type_id
        @product.accept=false
        if @product.save
            # r=Paysure::GetToken.new({
            #     amount: 25000,
            #     callback_url: "http://localhost:3000/callBackUpgrade?pos=nardeban&product_id=#{params[:id]}"
            # })
            # res=r.call
            # res.response['redirect_to']

          redirect_to '/',flash: {product_id: @product.id, haraji: (@product.off_price.nil? or !@product.off_price.blank?)}
        else
          render('new_product') 
        end
      end



      def destroy_product
        current_user.products.find(params[:id]).destroy
        redirect_to '/'
      end


      def update_product
          @product=current_user.products.find(params[:id])
          @product.accept=false
          if @product.update_attributes(product_white_list)
            redirect_to '/products?me=products',product_id: @product.id
          else
            render('product') 
          end
       end



      def callBackUpgrade
        @product=current_user.products.find(params[:product_id])
        res=Paysure::Verify.new({
            amount: 10000,
            token: params[:token] 
        }).call

        if res.valid?
            if params[:pos] == 'fori'
             @product.update_attribute(:fori,true)
            elsif params[:pos] == 'nardeban'
             @product.update_attribute(:nardeban,true)
            elsif params[:pos] == 'link_website'
                @product.update_attribute(:paid_linkwebsite,true)
            end

            flash[:notice]='محصول با موفقیت ارتقاع پیدا کرد'
        else
            flash[:notice]='متاسفانه تراکنش مورد نظر نهایی نشد لطفا دوباره سعی کنید'
        end

        redirect_to product_show_url(@product.id)
      end 

      def upgrade_product
        @p=Product.find(params[:id])
        if params[:pos]=='berozresani'
            r=Mellat::BpPayRequest.new({
                orderId: 1,
                amount: 50000,
            })

            res=r.call
            # redirect_to res.redirect_to
        elsif params[:pos]=='fori'
            r=Paysure::GetToken.new({
                amount: 60000.to_s,
                callback_url: "https://pplaza.ir/callBackUpgrade?pos=fori&product_id=#{params[:id]}"
            })
            res=r.call
        elsif params[:pos]=='video'
            r=Paysure::GetToken.new({
                amount: 10000.to_s,
                callback_url: "https://pplaaza.com/callBackUpgrade?pos=video&product_id=#{params[:id]}"
            })
            res=r.call
        elsif params[:pos]=='haraji'
            r=Paysure::GetToken.new({
                amount: 25000.to_s,
                callback_url: "https://pplaaza.com/callBackUpgrade?pos=haraji&product_id=#{params[:id]}"
            })
            res=r.call
        elsif params[:pos]=='berozresanivatamdid'
            r=Paysure::GetToken.new({
                amount: 50000.to_s,
                callback_url: "https://pplaaza.com/callBackUpgrade?pos=fori&product_id=#{params[:id]}"
            })
            res=r.call

        elsif params[:pos]=='berozresanivavijeh'
            r=Paysure::GetToken.new({
                amount: 60000.to_s,
                callback_url: "https://pplaaza.com/callBackUpgrade?pos=fori&product_id=#{params[:id]}"
            })
            res=r.call
        elsif params[:pos]=='tamdid'
            r=Paysure::GetToken.new({
                amount: 10000.to_s,
                callback_url: "https://pplaaza.com/callBackUpgrade?pos=link_website&product_id=#{params[:id]}"
            })
            res=r.call
        elsif params[:pos] =='link_website'
            r=Paysure::GetToken.new({
                amount: 150000.to_s,
                callback_url: "https://pplaaza.com/callBackUpgrade?pos=link_website&product_id=#{params[:id]}"
            })
            res=r.call
        end

return render json: res

        render html:
        ('
        <p style="
        display: block;
        text-align: center;
        position: fixed;
        left: 38%;
        top: 41%;
        font-size: 21px;
        direction: rtl;
        z-index: 999999999999999999999999;">
            در حال انتقال به درگاه پرداخت ...
        </p>

        <style>
        .cssload-wraper{
            width:0px;
            display:block;
            margin: 138px auto;
        }
            
        .cssload-dots {
            font-family: sans-serif;
            font-weight: 100;
        }
        
        .cssload-dots:not(:required) {
            overflow: hidden;
            position: relative;
            text-indent: -13749px;
            display: inline-block;
            width: 10px;
            height: 10px;
            background: transparent;
            border-radius: 100%;
            box-shadow: rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
                -o-box-shadow: rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
                -ms-box-shadow: rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
                -webkit-box-shadow: rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
                -moz-box-shadow: rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            animation: cssload-dots 2.5s infinite ease-in-out;
                -o-animation: cssload-dots 2.5s infinite ease-in-out;
                -ms-animation: cssload-dots 2.5s infinite ease-in-out;
                -webkit-animation: cssload-dots 2.5s infinite ease-in-out;
                -moz-animation: cssload-dots 2.5s infinite ease-in-out;
            transform-origin: 50% 50%;
                -o-transform-origin: 50% 50%;
                -ms-transform-origin: 50% 50%;
                -webkit-transform-origin: 50% 50%;
                -moz-transform-origin: 50% 50%;
        }
        
        
        
        @keyframes cssload-dots {
            0% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            8.33% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            16.67% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px 19px 0 10px, rgb(255,204,102) 19px 19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            25% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            33.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee -19px -19px 0 10px;
            }
        
            41.67% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            50% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            58.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            66.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px -19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            75% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            83.33% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee 19px 19px 0 10px;
            }
        
            91.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            100% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        }
        
        @-o-keyframes cssload-dots {
            0% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            8.33% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            16.67% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px 19px 0 10px, rgb(255,204,102) 19px 19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            25% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            33.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee -19px -19px 0 10px;
            }
        
            41.67% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            50% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            58.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            66.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px -19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            75% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            83.33% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee 19px 19px 0 10px;
            }
        
            91.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            100% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        }
        
        @-ms-keyframes cssload-dots {
            0% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            8.33% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            16.67% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px 19px 0 10px, rgb(255,204,102) 19px 19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            25% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            33.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee -19px -19px 0 10px;
            }
        
            41.67% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            50% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            58.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            66.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px -19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            75% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            83.33% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee 19px 19px 0 10px;
            }
        
            91.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            100% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        }
        
        @-webkit-keyframes cssload-dots {
            0% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            8.33% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            16.67% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px 19px 0 10px, rgb(255,204,102) 19px 19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            25% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            33.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee -19px -19px 0 10px;
            }
        
            41.67% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            50% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            58.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            66.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px -19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            75% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            83.33% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee 19px 19px 0 10px;
            }
        
            91.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            100% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        }
        
        @-moz-keyframes cssload-dots {
            0% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) -19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            8.33% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px -19px 0 10px, rgb(255,204,102) 19px -19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            16.67% {
                box-shadow: white 0 0 21px 0, rgb(255,136,102) 19px 19px 0 10px, rgb(255,204,102) 19px 19px 0 10px, rgb(102,221,119) 19px 19px 0 10px, rgb(68,170,238) -19px 19px 0 10px;
            }
        
            25% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            33.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee -19px -19px 0 10px;
            }
        
            41.67% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            50% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            58.33% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 -19px 19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            66.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 -19px -19px 0 10px, #66dd77 -19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            75% {
                box-shadow: white 0 0 21px 0, #ff8866 19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px -19px 0 10px, #44aaee 19px -19px 0 10px;
            }
        
            83.33% {
                box-shadow: white 0 0 21px 0, #ff8866 19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee 19px 19px 0 10px;
            }
        
            91.67% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px 19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        
            100% {
                box-shadow: white 0 0 21px 0, #ff8866 -19px -19px 0 10px, #ffcc66 19px -19px 0 10px, #66dd77 19px 19px 0 10px, #44aaee -19px 19px 0 10px;
            }
        }
        </style>

        <div class="cssload-wraper">
	<div class="cssload-dots"></div>
</div>


        <style>
        #wall {
	position:relative;
	width: 100%;
	height: 263px;
	top: -18px;
	background: rgb(255,255,255);
	text-align: center;
	padding-top: 53px;
	color: rgb(241,81,37);
}

#eye-l, #eye-r {
	position:absolute;
	z-index: 20;
	width: 18px;
	height: 18px;
	border-radius: 50%;
	background: rgb(255,255,255);
	margin-top:9px;
	left: 9px;
	margin-left: -9px;
	animation: search 3.5s infinite;
		-o-animation: search 3.5s infinite;
		-ms-animation: search 3.5s infinite;
		-webkit-animation: search 3.5s infinite;
		-moz-animation: search 3.5s infinite;
	box-sizing: border-box;
		-o-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
	border: 5px solid rgb(241,81,37);
}

#eye-r {
	margin-left: 9px;
}

#nose {
	position:relative;
	width: 26px;
	height: 26px;
	border: 5px solid rgb(241,81,37);
	border-radius: 50%;
	border-top-color: transparent;
	background: rgb(255,255,255);
	top:21px;
	left:-12px;
	transform: rotate(35deg);
		-o-transform: rotate(35deg);
		-ms-transform: rotate(35deg);
		-webkit-transform: rotate(35deg);
		-moz-transform: rotate(35deg);
	animation: noser 3.5s infinite;
		-o-animation: noser 3.5s infinite;
		-ms-animation: noser 3.5s infinite;
		-webkit-animation: noser 3.5s infinite;
		-moz-animation: noser 3.5s infinite;
}

#mouth{
	position:relative;
	width: 11px;
	height: 11px;
	border-radius: 50%;
	background: rgb(241,81,37);
	margin-top:35px;
	float:left;
	margin-left: 5px;
	animation: search 3.5s infinite;
		-o-animation: search 3.5s infinite;
		-ms-animation: search 3.5s infinite;
		-webkit-animation: search 3.5s infinite;
		-moz-animation: search 3.5s infinite;
}

#man {
    position: fixed;
    bottom: 0;
    right: 21px;
    width: 88px;
	height: 123px;
	border: 9px solid rgb(241,81,37);
	border-radius: 50%;
	margin-left:50%;

	animation: pop 14s infinite;
		-o-animation: pop 14s infinite;
		-ms-animation: pop 14s infinite;
		-webkit-animation: pop 14s infinite;
		-moz-animation: pop 14s infinite;
}







@keyframes search {
	0%, 100% { transform:translate(0px, 0px);}
	50% {transform:translate(56px, 0px);}
}

@-o-keyframes search {
	0%, 100% { -o-transform:translate(0px, 0px);}
	50% {-o-transform:translate(56px, 0px);}
}

@-ms-keyframes search {
	0%, 100% { -ms-transform:translate(0px, 0px);}
	50% {-ms-transform:translate(56px, 0px);}
}

@-webkit-keyframes search {
	0%, 100% { -webkit-transform:translate(0px, 0px);}
	50% {-webkit-transform:translate(56px, 0px);}
}

@-moz-keyframes search {
	0%, 100% { -moz-transform:translate(0px, 0px);}
	50% {-moz-transform:translate(56px, 0px);}
}

@keyframes noser {
	0%, 100% { transform:translate(0px, 0px) rotate(35deg);}
	50% {transform:translate(75px, 0px) rotate(-35deg);}
}

@-o-keyframes noser {
	0%, 100% { -o-transform:translate(0px, 0px) rotate(35deg);}
	50% {-o-transform:translate(75px, 0px) rotate(-35deg);}
}

@-ms-keyframes noser {
	0%, 100% { -ms-transform:translate(0px, 0px) rotate(35deg);}
	50% {-ms-transform:translate(75px, 0px) rotate(-35deg);}
}

@-webkit-keyframes noser {
	0%, 100% { -webkit-transform:translate(0px, 0px) rotate(35deg);}
	50% {-webkit-transform:translate(75px, 0px) rotate(-35deg);}
}

@-moz-keyframes noser {
	0%, 100% { -moz-transform:translate(0px, 0px) rotate(35deg);}
	50% {-moz-transform:translate(75px, 0px) rotate(-35deg);}
}

@keyframes pop {
	0%, 100% { transform:translate(0px, 140px)}
	20%, 80% { transform:translate(0px, 18px)}
}

@-o-keyframes pop {
	0%, 100% { -o-transform:translate(0px, 140px)}
	20%, 80% { -o-transform:translate(0px, 18px)}
}

@-ms-keyframes pop {
	0%, 100% { -ms-transform:translate(0px, 140px)}
	20%, 80% { -ms-transform:translate(0px, 18px)}
}

@-webkit-keyframes pop {
	0%, 100% { -webkit-transform:translate(0px, 140px)}
	20%, 80% { -webkit-transform:translate(0px, 18px)}
}

@-moz-keyframes pop {
	0%, 100% { -moz-transform:translate(0px, 140px)}
	20%, 80% { -moz-transform:translate(0px, 18px)}
}
        </style>
        <div id="man">
	<div id="eye-l"></div>
	<div id="eye-r"></div>
	<div id="nose"></div>
	<div id="mouth"></div>
</div>
<div id="wall"></div>

        ' + "
        <script>
        window.location=#{res.response['redirect_to']};
        </script>").html_safe
        # redirect_to res.response['redirect_to']

      end

def products
    if params[:me].nil?
    @products=Product.where(accept: true).where(['created_at > ?', 30.days.ago])
    @files = Dossier.all
    else
        @products=current_user.products
        @files = Dossier.all.where(['created_at > ?', 30.days.ago])
    end

    unless params[:product_vijeh].nil?
        @products=Product.where(fori: true).where(['created_at > ?', 30.days.ago])
    end

    unless params[:product_offer].nil?
        @products=Product.where(off_price_paid: true).where(['created_at > ?', 30.days.ago])
    end

end
    def file
        @file=Dossier.find(params[:id])
        @exposition=@file.user
    end
    def favorite
        begin 
    if params[:liked]=='false'
      current_user.favorites.build(product_id: params[:id]).save
    else
      current_user.favorites.find_by(product_id: params[:id]).delete
    end
    rescue 
    
    end
    render text: current_user.favorites.exists?(product_id: params[:id])
    
    
    
    end
    def favorites
       @products=current_user.favorite_produts
    end
    
    def search 
       if params[:post_type]=='product'
           @products = Product.where("lower(name) LIKE ?", "%#{params[:q].downcase}%")
           @products = @products.where(state: params[:state]) unless params[:state].blank? or (params[:state].to_i == 0)
       else
           @expositions=User.sellers.where("lower(exposition_name) LIKE ? OR lower(name) LIKE ?", "%#{params[:q].downcase}%","%#{params[:q].downcase}%") 
       end
    end
    
    
    def login
        
        @user=User.where('phone = ? OR email = ?',params[:email],params[:email]).first
        if @user.valid_password?(params[:password])
            flash[:notice]=[5000,'شما با موفقیت وارد شدید']
           sign_in(@user) 
           
        else
            flash[:notice]=[5000,'ایمیل یا رمز عبور نامعتبر است']
            redirect_to :back
        end
        
    end
    
    
    def save_exposition
        @user = User.new(exposition_white_list)
        @user.level = 1
        if @user.save
            redirect_to root_path
        else 
            render 'register_exposition'
        end
    end
    
    
    def page
       @page=Page.find_by_permalink(params[:permalink]) 
    end
    
    
    
    private
    
    def exposition_white_list
        params.require(:user).permit(:longitude,:latitude,:identify,:password,:password_confirmation,:static_phone,:avatar,:background_image,:category_id,:email,:name,:phone,:exposition_name,:exposition_detail,:exposition_address,:instagram_id,:telegram,:post_service)
    end
    def product_white_list
        params.require(:product).permit(:category_id,:address,:district,:city,:state,:name, :product_type_id,:price,:off_price,:detail, {images: []}).tap do |whitelisted|
          whitelisted[:properties] = params[:product][:properties]
        end
    end  
end
