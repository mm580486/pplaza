class Admin::ProductsController < ApplicationController
  layout 'admin'
  before_action :admin_and_mekter_auth
  before_action :find_product, :only => [:show,:edit,:destroy,:delete,:update,:toggle_lock]
  
    def index
       if params[:unaccept]=='true'
         @products=Product.where(:accept => false)
       else
         @products=Product.all 
       end
       if params[:vijeh] == 'true'
        @products= @products.where(fori: true)
       end
    end

    def block
      @product=Product.find(params[:id])
    end

    def action_block
      @product=Product.find(params[:id])
      @product.update_attributes(product_white_list)
      redirect_to :back
    end
    
    def toggle_lock
         if @product.update_attribute(:accept,!@product.accept?)
    flash[:notice]=[5000,t("admin.toast.product_accepted_#{@product.accept?}")]
  else
    
    flash[:notice]=[5000,@product.errors]
  end
    redirect_to :back
    
    
    end
    
    def delete
    if @product.destroy
      flash[:notice]=[5000,t('admin.toast.product_deleted')]
      redirect_to :back
    else
      flash[:notice]=[5000,@product.errors]
     redirect_to :back
    end
    end
    
    private 
    
    def product_white_list
      params.require(:product).permit(:name,:blocked_message,:price)
    end
    
    def find_product
      @product=Product.find(params[:id])
    end
    
end
