class Admin::SellerFilesController < ApplicationController
    layout 'admin'
    before_action :find_file,:only => [:edit,:delete,:update]
    def new
      @file=Dossier.new()
    end
    
    def create
      @file=current_user.dossiers.new(file_white_list)

      @file.approved=false
      if @file.save
        flash[:notice]=[5000,t('admin.toast.product_create')]
        redirect_to admin_seller_files_path
      else
        render('new') 
      end
    end
    
    def delete
      if @file.destroy 
      flash[:notice]=[5000,t('admin.toast.product_deleted')]
      redirect_to :back
    end
    end
    

  
    def index
      @files=current_user.dossiers
    end
  
    def edit
      
    end
    
    def update
    if @file.update_attributes(file_white_list)
    flash[:notice]=[5000,t("admin.toast.product_updated")]
    redirect_to admin_seller_file_path
  else
    render 'edit'
  end
  end
  
    def show
    end
      private
      
      def find_file
        @file=Dossier.find(params[:id])
      end
  
    def file_white_list
      params.require(:dossier).permit(:file_category_id,:name,:price,:off_price,:detail, :file)
      end
end
