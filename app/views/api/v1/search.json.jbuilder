unless @products.nil?
json.products @products do |product|
  json.id product.id
  json.name product.name
  json.exposition_name product.user.name
  json.exposition_id product.user.id
  json.avatar product.user.avatar_url.nil? ? "https://pplaaza.com#{@setting.default_image_exposition}" : "https://pplaaza.com#{product.user.avatar_url}" 
  json.poster "https://pplaaza.com#{product.images[0].url rescue "#{@setting.default_image_product}" }"
  begin
  json.price product.price.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
  json.off_price product.off_price.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
rescue
  json.price product.price
  json.off_price product.off_price
end
  json.has_offer_price !product.off_price.blank?
end
else

json.expositions @expositions do |user|
  json.id user.id
  json.name user.name
  json.exposition_name user.exposition_name
  json.background user.background_image_url.nil? ? "https://pplaaza.com#{Setting.first.default_image_exposition_url}" : "https://pplaaza.com#{user.background_image_url}"
  json.avatar user.avatar_url.nil? ? Setting.first.default_image_exposition_url : "https://pplaaza.com#{user.avatar_url}"
  json.static_phone user.static_phone
  json.instagram user.instagram_id
  json.telegram user.telegram
  json.exposition_details user.exposition_detail
  json.exposition_address user.exposition_address
  json.post_service user.post_service?
  json.products_size user.products.size
  json.followers_size user.followers.size
  json.rate (user.votes.sum(:vote).to_f / user.votes.size.to_f) 
  if @seener
  json.followed user.followers.include? @seener 
  end

end

end