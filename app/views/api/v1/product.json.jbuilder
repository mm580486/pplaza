  json.id @product.id
  json.name @product.name
  if @product.images.length >= 1
  json.images @product.images.map {|image| "https://pplaaza.com#{image.url}" }
  else
    json.images ["https://pplaaza.com#{Setting.first.default_image_product_url}"]
  end
  begin
  json.price @product.price.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
  json.off_price @product.off_price.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
rescue
  json.price @product.price
  json.off_price @product.off_price
end
  json.has_offer_price !@product.off_price.blank?
  json.exposition_name @product.user.exposition_name
  json.exposition_id @product.user.id
  json.exposition @product.user.exposition?
  json.avatar "https://pplaaza.com#{@product.user.avatar.url}"
  json.detail @product.detail
  json.category Category.find(@product.category_id).name
  json.dynamic_fields @product.properties.map {|key,value| "#{key}:#{value}"}
  if @user
  json.favorited @user.favorites.exists?(product_id: @product.id)
  end
json.phone @product.user.phone


  json.user_product @user.nil? ? false : @user.products.exists?(@product.id)