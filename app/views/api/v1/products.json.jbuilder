json.products @products do |product|
  json.id product.id
  json.name product.name
  json.exposition_name product.user.name rescue nil
  json.exposition_id product.user.id rescue nil
  json.exposition product.user.exposition? rescue false
  json.detail truncate(product.detail, :length => 50, :omission => '... (برای ادامه کلیک کنید)')
  json.views product.view_product

  json.state state(product.state) rescue nil
  json.created_at JalaliDate.new(product.created_at).strftime("%A %d %b")
  json.avatar (product.user.avatar_url.nil? ? "https://pplaaza.com#{@setting.default_image_exposition}" : "https://pplaaza.com#{product.user.avatar_url}" ) rescue "https://pplaaza.com#{@setting.default_image_exposition}"
  json.poster "https://pplaaza.com#{product.images[0].url rescue "#{@setting.default_image_product}" }"
begin
  json.offer_p 'حراجی'
  #product.off_price.ctsd.to_i.percent_of(product.price.ctsd.to_i) rescue 0
rescue
  json.offer_p 0
end


    begin
  json.price product.price.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
  json.off_price product.off_price.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse
rescue
  json.price product.price
  json.off_price product.off_price
end


  json.has_offer_price !product.off_price.blank?
  if @user
  json.favorited @user.favorites.exists?(product_id: product.id) 
  end
end