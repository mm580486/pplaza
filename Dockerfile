FROM ruby:2.4
LABEL maintainer="Mohammad Mahmoodi <mm580486@gmail.com>"
# Install packages for building ruby
RUN apt-get update
RUN apt-get update -qq && apt-get install -y apt-transport-https build-essential libpq-dev nodejs
RUN apt-get install -y postgresql postgresql-contrib libmagickwand-dev
RUN apt-get install optipng
ENV RAILS_ROOT /app/back/pplaaza/BE
ENV FRONT_ROOT /app/back/pplaaza/
ENV RAILS_ENV production
RUN mkdir -p $RAILS_ROOT
RUN mkdir -p $FRONT_ROOT
WORKDIR $RAILS_ROOT
ADD Gemfile ./Gemfile
ADD Gemfile.lock ./Gemfile.lock
RUN gem update --system
RUN gem update bundler
RUN gem install puma
COPY . .
RUN bundle install --without development test --binstubs
# Precompile assets.
# COPY ./init.d/sidekiq /etc/init.d/
# EXPOSE 80
CMD ["bin/secret"]
CMD rails db:migrate RAILS_ENV=production
CMD ["bin/rake db:seed"]


ENV PORT 5655
CMD puma -C config/puma.rb -p 5655