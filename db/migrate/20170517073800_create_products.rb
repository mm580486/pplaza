class CreateProducts < ActiveRecord::Migration
  def up
    create_table :products do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name,null: false
      t.integer :category_id
      t.string :price
      t.string :off_price
      t.boolean :off_price_paid,default: false
      t.integer :position
      t.boolean :comment,default: true
      t.boolean :accept,default: false
      t.integer :view_product,default: 0
      t.references :product_type, index: true, foreign_key: true
      t.text :properties, :hstore, default: {}
      t.column  :images, :string
      t.string :detail 
      t.timestamps null: false
      t.string :state
      t.string :city
      t.string :phone
      t.string :email
      t.string :district
      t.string :address
      t.boolean :fori ,default: false
      t.boolean :nardeban,default: false
      t.boolean :tamdid,default: false
      t.boolean :link_website,default: false
      t.boolean :tamdid_nardeban,default: false
      t.boolean :fori_nardeban,default: false
      t.boolean :paid_linkwebsite,default: false
      
      t.boolean :paid_video,default: false

      t.string :blocked_message,default: ''
      t.string :deleted_reason,default: ''
      t.boolean :deleted_by_user,default: false

    end
  end
  
  def down
    drop_table :products
  end
end
