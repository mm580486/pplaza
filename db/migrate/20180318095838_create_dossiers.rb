class CreateDossiers < ActiveRecord::Migration
  def up
    create_table :dossiers do |t|
      t.integer :user_id
      t.string :file
      t.string :name
      t.string :detail
      t.integer :file_category_id
      t.string :price
      t.string :off_price
      t.boolean :approved,default: false
      t.string :token
      t.timestamps null: false
    end
  end

  def down 
    drop_table :dossiers
  end
end
